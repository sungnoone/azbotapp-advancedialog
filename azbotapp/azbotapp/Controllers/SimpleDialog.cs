﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace azbotapp.Controllers
{
    [Serializable]
    public class SimpleDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var activity = await result as IMessageActivity;
            await context.PostAsync("翰林客服您好，請輸入服務代碼(1~3):");
            await context.PostAsync("<1>訂書 <2>查詢訂單 <3>線上客服");
            context.Wait(MessageReceivedOperationChoice);
        }

        private async Task MessageReceivedOperationChoice(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            if (message.Text.ToLower().Equals("1", StringComparison.InvariantCultureIgnoreCase))
            {
                context.Call<object>(new order(), AfterChildDialogIsDone);
            }
            else if (message.Text.ToLower().Equals("2", StringComparison.InvariantCultureIgnoreCase))
            {
                context.Call<object>(new query(), AfterChildDialogIsDone);
            }
            else if (message.Text.ToLower().Equals("3", StringComparison.InvariantCultureIgnoreCase))
            {
                context.Call<object>(new custom(), AfterChildDialogIsDone);
            }
            else
            {
                await context.PostAsync("輸入錯誤，請輸入正確服務代碼");
                await context.PostAsync("< 1 > 訂書 < 2 > 查詢訂單 < 3 > 線上客服");
                context.Wait(MessageReceivedAsync);//回原點
            }
        }

        private async Task AfterChildDialogIsDone(IDialogContext context, IAwaitable<object> result)
        {
            context.Wait(MessageReceivedAsync);
        }

    }
}