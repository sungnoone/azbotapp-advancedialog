﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace azbotapp.Controllers
{
    [Serializable]
    public class custom : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("請輸入您的問題");
            context.Wait(MessageReceivedCustom);
        }

        //客服
        private async Task MessageReceivedCustom(IDialogContext context, IAwaitable<object> result)
        {
            await context.PostAsync("線上客服結束");
            context.Done<object>(new object());
        }

    }
}