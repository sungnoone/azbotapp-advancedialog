﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace azbotapp.Controllers
{
    [Serializable]
    public class order : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("請輸入書名");
            context.Wait(MessageReceivedOrder);
        }


        //訂書
        private async Task MessageReceivedOrder(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            await context.PostAsync($"您輸入的書名是:{message.Text}");
            await context.PostAsync($"確定下單嗎?(y/n)");
        }

        //確認下單
        private async Task MessageReceivedOrderSure(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            if (message.Text.ToLower() == "y")
            {
                await context.PostAsync($"訂單成立感謝您!!");
            }
            else
            {
                await context.PostAsync($"訂單取消");

            }
            context.Done<object>(new object());
        }

    }
}