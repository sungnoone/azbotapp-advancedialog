﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace azbotapp.Controllers
{
    [Serializable]
    public class query : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("請輸入單號");
            context.Wait(MessageReceivedQuery);
        }

        //查單
        private async Task MessageReceivedQuery(IDialogContext context, IAwaitable<object> result)
        {
            await context.PostAsync("查詢訂單結束");
            context.Done<object>(new object());
        }
    }
}